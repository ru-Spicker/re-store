import React from 'react';
import { Link } from 'react-router-dom';

const CartPage = () => {
    return (
        <div>
            <nav>
                <Link to="/">Home</Link>
            </nav>
            <h1>CartPage</h1>
        </div>
    )
}

export default CartPage;