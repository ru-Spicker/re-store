import React from 'react';
import { Link } from 'react-router-dom';
import BookList from '../../book-list/book-list';

const HomePage = () => {
    return (
        <div>
            <nav>
                <Link to="/cart">Cart</Link>
            </nav>
            <BookList />
        </div>
    )
}

export default HomePage;