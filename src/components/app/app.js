import React from 'react';
import withBookstoreService from '../hoc';
import HomePage from '../pages/homepage';
import CartPage from '../pages/cartpage';
import Page404 from '../pages/page404';
import { Link, Switch, Route } from 'react-router-dom';

import "./app.css"

const App = ({ bookstoreService }) => {
    console.log(bookstoreService.getBooks())
    return (
    <div>
        <nav>
            <ul>
            <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/cart">Cart</Link>
                </li>
            </ul>
        </nav>
        <h1>App</h1>
        <Switch>
            <Route path="/"
                component={HomePage}
                exact />
            <Route path="/cart"
                component={CartPage} />
            <Route
            component={Page404} />
        </Switch>
    </div>
    )
};

export default withBookstoreService()(App);