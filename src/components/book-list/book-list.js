import React, { Component } from 'react';
import BookListItem from '../book-list-item/';


export default class BookList extends Component {
    render () {
        // const { books } = this.props;
        const books = [
            {
                id: 0,
                title: 'title0',
                author: 'autor0'
            },
            {
                id: 1,
                title: 'title1',
                author: 'autor1'
            },
        ];
    
            return (
            <ul>
                {
                    books.map((book) => {
                        return(
                        <li key={book.id}>
                            <BookListItem book={book}/>
                        </li>
                        )
                    })
                }
            </ul>
        )
    }
}